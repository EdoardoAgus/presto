<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use App\Models\ArticleImage;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;
    use Searchable;
    protected $fillable = [
        'name',
        'description',
        'price',
        'img',
        'contact',
        'category_id',
        'user_id',
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    static public function ToBeRevisionedCount(){
        return Article::where('is_accepted', null)->count();
    }
    static public function ToBeTrashedCount(){
        return Article::where('is_accepted', false)->count();
    }

    public function preview($anteprima){
        $anteprima = strip_tags($anteprima);
        $anteprima = substr($anteprima, 0 , 50);
        $anteprima = $anteprima."...";
        return $anteprima;
    }
    public function getPreview(){
        return $this->preview($this->body);
    }

    public function toSearchableArray()
    		{
        
		$categoria=$this->category;
    	$array = [
			"id" => $this->id,
			"name" => $this->name,
			"description" => $this->description,
			"altro"=> "article category",//forse in italiano
			"categoria"=>$categoria,
			];
        return $array;
    		}
    public function images() {
    return $this->hasMany(ArticleImage::class);
            }
}
