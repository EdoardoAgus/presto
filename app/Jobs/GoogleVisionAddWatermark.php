<?php

namespace App\Jobs;

use App\Models\Article;
use Spatie\Image\Image;
use App\Models\ArticleImage;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use App\Jobs\GoogleVisionAddWatermark;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class GoogleVisionAddWatermark implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $article_image_id;

    public function __construct($article_image_id)
    {
        $this->article_image_id = $article_image_id;
    }


    public function handle()
    {
        $i = ArticleImage::find($this->article_image_id);
		if(!$i) {
		return;
        }
    $srcPath = storage_path('/app/' . $i->file);
    $image = file_get_contents($srcPath);
   

     $image = Image::load($srcPath);
        $image
        ->watermark(base_path('public/img/watermark.png'))
        ->watermarkPadding(50)
        ->watermarkHeight(20, Manipulations::UNIT_PERCENT)
        ->watermarkWidth(45, Manipulations::UNIT_PERCENT)
        ->watermarkPosition(Manipulations::POSITION_BOTTOM_LEFT);

    $image->save($srcPath);
    }
}



