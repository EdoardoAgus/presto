<?php

namespace App\Http\Controllers;

use Throwable;
use App\Models\Article;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\ArticleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests\ArticleRequest;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use League\Glide\Manipulators\Watermark;
use App\Jobs\GoogleVisionAddWatermark;

class ArticleController extends Controller
{   
    
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    public function uploadImages(Request $request)
	{
	$uniqueSecret = $request->input('uniqueSecret');
	$fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");
    dispatch(new ResizeImage(
        $fileName,
            80,
            80
    ));

	session()->push("images.{$uniqueSecret}", $fileName);
	// return response()->json (session()->get("images.{$uniqueSecret}"));	
    return response()->json(
        [
        'id'=>$fileName
        ]);
    }

    public function removeImage(Request $request)
	{
	$uniqueSecret = $request->input('uniqueSecret');
	$fileName = $request->input('id');
	session()->push("removedimages.{$uniqueSecret}", $fileName);
	Storage::delete($fileName);
	return response()->json('ok');
	}
    
    public function getImages(Request $request)
	{
		$uniqueSecret = $request->input('uniqueSecret');
		$images = session()->get("images.{$uniqueSecret}", []);
		$removedImages = session()->get("removedImages.{$uniqueSecret}", []);
		$images = array_diff($images, $removedImages);
 		$data = [];
 
		foreach ($images as $image) {
			$data[] = [
				'id' => $image,
				'src' => ArticleImage::getUrlByFilePath($image, 80, 80),
				];
	}
	return response()->json($data);
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $categories = Category::find(1);
        $uniqueSecret = $request->old('uniqueSecret', base_convert(sha1(uniqid(mt_rand())), 16, 36));   
        $categories = Category::all();
        // dd($categories);
        return view('article.create',compact('categories', 'uniqueSecret'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $category=Category::find($request->category);
        // $article = Article::find($article);
       
        $article = Article::create([
                'name'=>$request->name,
                'description'=>$request->description,
                'price'=>$request->price,
                // 'img'=>$request->file('img')->store('public/img'),
                'contact'=>$request->contact,
                'category_id'=>$request->category,
                'user_id'=>Auth::id(),
                ]);
        // $category->articles()->create([
        //     'name'=>$request->name,
        //     'description'=>$request->description,
        //     'price'=>$request->price,
        //     // 'img'=>$request->file('img')->store('public/img'),
        //     'contact'=>$request->contact,
        //     'category'=>$request->category,
        //     'user_id'=>Auth::id(),

        // ]);   
        // dd($request->all());
            $uniqueSecret = $request->uniqueSecret;
            $images = session()->get("images.{$uniqueSecret}",[]);
            $removedImages = session()->get("removedImages.{$uniqueSecret}",[]);
            $images = array_diff($images, $removedImages);
            foreach ($images as $image) {
            $i = new ArticleImage();
            $fileName = basename($image);
            $newFileName = "public/articles/{$article->id}/{$fileName}";
            Storage::move($image, $newFileName);

            $i ->file =$newFileName;
            $i->article_id=$article->id;

            $i->save();

            Bus::chain([
                new GoogleVisionSafeSearchImage($i->id),
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new GoogleVisionAddWatermark($i->id),
                new ResizeImage($i->file, 300, 150),
                new ResizeImage($i->file, 400,300) ])->dispatch($i->id);
            }
            File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
            
            return redirect(route('home'))->with('message','La tua Richiesta è stata inoltrata!');
        

        // $article->categories()->attach($request->category);     
        // precedente
        // 
            
        // dd($request->category);   
        // dd($request);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    
    public function show(Article $article)
    {
        
        // dd($article);
        // $categories = Category::all();
        return view('article.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect(route('revisor.trash'));
    }
    
}
