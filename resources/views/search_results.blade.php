<x-layout>
    <x-navbar>

        <div class="container my-5">
            <div class="row">
                <div class="col-12 category-border">
                    <h2 class="text-center tc-sec pt-4 mt-2">{{$q}}</h2>
                </div>
                @foreach ($articles as $article)
                <div class="col-12 col-md-6">
                        <div class="card card-custom my-5 mx-5">
                            <div class="card-body">
                                
                                {{$article->body}}

                                @foreach ($article->images as $image)
                                @if ($loop->first)
                                    <img class="img-fluid "
                                        src="{{ $image->getUrl(300, 150) }}" alt="">
                                @endif

                                {{ $article->body }}


                            @endforeach

                                <h4 class="card-text fw-bold">{{ $article->name }}</h4>
                                <h5 class="card-text fw-bold text-truncate">{{ $article->description }} </h5>
                                <h5 class="card-text fw-bold">{{ $article->price }} $</h5>
                                <h5 class="card-text fw-bold"> {{$article->category->name}}</h5>
                                <p>{{__('ui.creato il')}} {{$article->created_at->format('d/m/y')}} {{__('ui.da')}} {{$article->user->name}}</p>
                                <a href="{{ route('article.show', compact('article')) }}"
                                    class="btn btn-primary btn-custom">{{ __('ui.dettaglio') }}</a>
                            </div>
                            <i class="fas fa-carrot card-carrot "></i>
                        </div>
                </div>
                @endforeach
                <div class="row justify-content-center">
                </div>
            </div>
        </div>
    </x-navbar>
</x-layout>