<x-layout>
    <x-navbar>

        @if (count($articles)>0)
            <div class="col-12 ">
                <h2 class="text-center tc-sec pt-4 mt-5 "> <span class="category-border px-5"> Cestino</span></h2>
            </div>
            @foreach ($articles as $article)
                <div class="container my-2 py-5">
                    <div class="row justify-content-center">
                        <div class="col-12 tc-accent2 fs-5 fw-bold text-center border ">
                            Annuncio N° {{ $article->id }}
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="card card-custom-trash my-5 mx-2">
                                <div class="card-body">
                                    @foreach ($article->images as $image)
                                    {{-- @if($loop->first)   --}}
                                    <img class="img-fluid " src="{{$image->getUrl(300, 150)}}" alt="">
                                    {{-- @endif --}}
                                    
                                    {{$article->body}}
                                    
                                        
                                    @endforeach
                                    <h4 class="card-text fw-bold">{{ $article->name }}</h4>
                                    <h5 class="card-text fw-bold text-truncate ">{{ $article->description }}</h5>
                                    <h5 class="card-text fw-bold">{{ $article->price }} $</h5>
                                    <a
                                        href="{{ route('category.articles', [$article->category->name, $article->category->id]) }}">
                                        {{ $article->category->name }}</a>
                                    {{-- ['cat'=>$article->category_id] [ $article->category->name, $article->category->id]) --}}
                                    <p>{{__('ui.creato il')}} {{ $article->created_at->format('d/m/y') }} {{__('ui.da')}}
                                        {{ $article->user->name }}
                                    </p>
                                    <div class="col-12 d-flex justify-content-around"><a
                                            href="{{ route('article.show', compact('article')) }}"
                                            class="btn btn-primary btn-custom">{{__('ui.dettaglio')}}</a>
                                        <form action="{{ route('revisor.restore', $article->id) }}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-custom2 tc-sec">Restore</button>
                                        </form>
                                        <form action="{{ route('article.destroy', compact('article')) }}"
                                            method="POST">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-custom3 tc-sec" type="submit">{{__('ui.cancella')}}</button>
                                        </form>
                                    </div>
                                </div>
                                <i class="fas fa-carrot card-carrot "></i>
                            </div>
                        </div>
                    </div>

                </div>
            @endforeach
            </div>
        @else
            <div class="my-5 p-5 text-center">
                <h1>{{__('ui.titolo-trash')}}</h1>
                <h3>{{__('ui.sotto-trash')}}</h3>
            </div>
        @endif





    </x-navbar>
</x-layout>
