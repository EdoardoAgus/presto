<!-- logo nav  -->
<nav id="navbar-presto" class="navbar navbar-expand-lg d-flex fixed-top px-0 px-sm-5">
    <div class="container-fluid ">
        <div class="col-12">
            <div class="row justify-content-between align-items-center">
                <div class="col-3 col-md-2 d-flex">
                    <a class="navbarBrand" href="{{ route('home') }}">
                        Presto
                    </a>
                    <i class="icofont-rabbit icon-logo"></i>
                </div>
                <div class="col-6 d-flex appare-sm align-items-center justify-content-end">

                    <!-- navbar sm!! -->
                    <!-- bottone vendi i tuoi prodotti icona per sm -->
                    {{-- <li><div class="bg-dark"></div></li> --}}
                    <!-- fine logo nav -->
                    <!-- bottone menu sm -->
                    <div class="col-2 text-end  mx-3">
                        <button class="navbar-toggler btn-menu" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon menu-collapse pt-1 ps-0 ps-sm-5">
                                <i id="icon-menu" class="fas fa-ellipsis-v"></i>
                            </span>
                        </button>

                    </div>
                </div>



                <!-- navbar collapse -->


                <div class="collapse navbar-collapse bg-base col-8 justify-content-between" id="navbarSupportedContent">
                    <div class=" text-end my-4 appare-sm">
                        <ul class="navbar-nav align-items-center">
                            @guest
                                <li>
                                    <a class="fs-5 tc-sec" href="{{ route('login') }}"> {{ __('ui.Accedi') }}</a>
                                </li>
                                <li>
                                    <a class="fs-5 tc-sec" href="{{ route('register') }}">{{ __('ui.registrati') }}</a>
                                </li>
                                <li>
                                    <form action="{{ route('search') }}" method="GET" class="d-flex">
                                        <input class="form-control me-2 cerca-custom" type="text"
                                            placeholder="Cosa stai cercando.." aria-label="Search" name="q">
                                        <button class="btn-custom4" type="submit"><i class="fas fa-search"
                                                style="border-radius: 100% "></i></button>
                                    </form>
                                </li>
                            @else
                                {{-- admin sm --}}
                                <li>
                                    <div class="col-12 d-flex justify-content-center">
                                        <!-- search -->
                                        <form action="{{ route('search') }}" method="GET" class="d-flex my-2">
                                            <input class="form-control me-2 cerca-custom" type="text"
                                                placeholder="Cosa stai cercando.." aria-label="Search" name="q">
                                            <button class="btn-custom4" type="submit"><i class="fas fa-search"></i></button>
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    <a class="fs-5 tc-sec btn btn-custom2 px-5 my-2" href="{{ route('article.create') }}">
                                        {{ __('ui.vendi') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}" type="button"
                                        class="btn btn-custom2 tc-sec active px-5"
                                        onclick="event.preventDefault(); document.getElementById('form-logout2').submit();">
                                        {{ __('ui.logout') }}
                                    </a>

                                    <form method="POST" action="{{ route('logout') }}" id="form-logout2">
                                        @csrf
                                    </form>
                                </li>
                                {{-- revisor sm --}}
                                
                                    @if (Auth::user()->is_revisor)
                                        <a class="btn btn-custom2 tc-sec nav-link mx-1 my-2" href="{{ route('revisor.home') }}">
                                            {{ __('ui.revisore') }} <span class=" badge bg-warning">
                                                {{ \App\Models\Article::ToBeRevisionedCount() }}
                                            </span>
                                        </a>
                                        <a class="btn btn-custom2 nav-link tc-sec mx-1 my-2" href="{{ route('revisor.trash') }}">
                                            {{ __('ui.cestino') }} <span
                                                class="counter">{{ \App\Models\Article::ToBeTrashedCount() }}</span></a>
                                    @endif

                                {{-- fine revisor sm --}}
                                
                                <li class="d-flex justify-content-center my-2">
                                    <button class="btn btn-custom2 dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        {{ ucfirst(app()->getLocale()) }}
                                    </button>
                                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuButton1" style="right:0px">
                                        <span></span>
                                        <li class="flag-it bg-base rounded-pill"><a class="dropdown-item" href="#">
                                                @include('components.locale',
                                                ['lang'=>'it','nation'=>'IT'])</a></li></span>
                                        <li class="flag-en bg-base rounded-pill "><a class="dropdown-item" href="#">
                                                @include('components.locale',
                                                ['lang'=>'en','nation'=>'GB'])</a></li>
                                        <li class="flag-es bg-base rounded-pill"><a class="dropdown-item" href="#">
                                                @include('components.locale',
                                                ['lang'=>'es','nation'=>'ES'])</a></li>
                                    </ul>
                                 {{-- fine admin --}}
                                </li>
                            </ul>
                        @endguest
                    </div>
                    <div class="col-4 sparisce-sm">
                        <!-- search -->
                        <form action="{{ route('search') }}" method="GET" class="d-flex">
                            <input class="form-control me-2 cerca-custom" type="text"
                                placeholder="{{ __('ui.search') }}" aria-label="Search" name="q">
                            <button class="btn-custom4" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>


                    <!-- button accedi/vai agli annunci    -->


                    <div class="col-7 d-flex justify-content-around sparisce-sm">
                        <div class="d-flex">
                            @guest
                                <!-- Button accedi iscriviti -->
                                {{-- <a href="{{ route('login') }}" type="button" class="btn btn-custom2 mx-3 tc-sec active">
                                    Accedi
                                </a>
                                <a href="{{ route('register') }}" type="button"
                                    class="btn btn-custom2 me-2 tc-sec active">
                                    Iscriviti
                                </a> --}}

                                <button type="button" class="btn btn-custom2 tc-sec active" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal">
                                    {{ __('ui.Accedi||iscriviti') }}
                                </button>


                            @else


                                {{-- revisor btn --}}
                                @if (Auth::user()->is_revisor)
                                    <a class="btn btn-custom2 tc-sec nav-link mx-1" href="{{ route('revisor.home') }}">
                                        {{ __('ui.revisore') }} <span class=" badge bg-warning">
                                            {{ \App\Models\Article::ToBeRevisionedCount() }}
                                        </span>
                                    </a>
                                    <a class="btn btn-custom2 nav-link tc-sec mx-1" href="{{ route('revisor.trash') }}">
                                        {{ __('ui.cestino') }} <span
                                            class="counter">{{ \App\Models\Article::ToBeTrashedCount() }}</span></a>
                                @endif
                                {{-- fine revisor btn --}}



                                <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                                    <ul class="navbar-nav">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle btn-custom2" href="#"
                                                id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown"
                                                aria-expanded="false">
                                                {{ __('ui.ciao') }} {{ Auth::user()->name }}
                                            </a>
                                            <ul class="dropdown-menu p-1" aria-labelledby="navbarDarkDropdownMenuLink">
                                                <li>
                                                    <button class="btn-custom my-1 tc-sec">
                                                        <a class="nav-link nav-li-cust "
                                                            href="{{ route('article.create') }}">
                                                            {{ __('ui.vendi') }}
                                                        </a>
                                                    </button>
                                                </li>
                                                <li>
                                                    <a href="{{ route('logout') }}" type="button"
                                                        class="btn btn-custom2 text-center nav-link me-2 tc-sec active "
                                                        onclick="event.preventDefault(); document.getElementById('form-logout').submit();">
                                                        {{ __('ui.logout') }}
                                                    </a>

                                                    <form method="POST" action="{{ route('logout') }}" id="form-logout">
                                                        @csrf
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endguest
                    <div class="btn-group d-flex justify-content-end my-0 sparisce-sm">
                        <button class="btn btn-custom2 dropdown-toggle" type="button" id="dropdownMenuButton1"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            {{ ucfirst(app()->getLocale()) }}
                        </button>
                        <ul class="dropdown-menu " aria-labelledby="dropdownMenuButton1">
                            <li class="flag-it"><a class="dropdown-item" href="#"> @include('components.locale',
                                    ['lang'=>'it','nation'=>'IT'])</a></li></span>
                            <li class="flag-en "><a class="dropdown-item" href="#"> @include('components.locale',
                                    ['lang'=>'en','nation'=>'GB'])</a></li>
                            <li class="flag-es "><a class="dropdown-item" href="#"> @include('components.locale',
                                    ['lang'=>'es','nation'=>'ES'])</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>

</nav>
<!-- modal -->


@if ($errors->any())
<div class="mt-5 pt-1 ">
<div class="rounded-circle alert fs-3 tc-base text-center alert-danger mt-5 pt-5">
        <ul>
            @foreach ($errors->all() as $error)
                <li> <p>{{ $error }}</p></li>
            @endforeach
        </ul>
    </div>
</div>
@endif
{{-- <div class="mt-5 pt-1 ">
    <div class="rounded-circle alert fs-3 tc-base text-center alert-danger mt-5 pt-5">
        <p class="text-middle">{{ ucfirst(session('email')) }}</p>                      
    </div>
</div> --}}
<div class="modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('ui.Accedi') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group py-2 tc-sec">
                        <label for="exampleInputEmail1 ">{{ __('ui.Indirizzo Email') }}</label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail1"
                            aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group py-2 tc-sec">
                        <label for="exampleInputPassword1 ">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                            placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-custom tc-base">{{ __('ui.Accedi') }}</button>

                </form>
            </div>
            <div class="modal-footer">
                <p>{{ __('ui.non sei ancora') }}</p>
                <button type="button" class="btn btn-custom2 tc-sec active" data-bs-toggle="modal"
                    data-bs-target="#exampleModa2">
                    {{ __('ui.iscriviti') }}
                </button>

            </div>
        </div>
    </div>
</div>
<!-- fine modal1 -->
<!--inizio modale 2-->
<div class="modal" id="exampleModa2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('ui.iscriviti') }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group py-2 tc-sec">
                        <label for="name2">{{ __('ui.nome utente') }}</label>
                        <input type="text" class="form-control" name="name" id="name2" placeholder="Nome e Cognome">
                    </div>
                    <div class="form-group py-2 tc-sec">
                        <label for="exampleInputEmail2">Indirizzo Email</label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail2"
                            aria-describedby="emailHelp" placeholder="Email">
                    </div>
                    <div class="form-group py-2 tc-sec">
                        <label for="exampleInputPassword2">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputPassword2"
                            placeholder="Password">
                    </div>
                    <div class="form-group py-2 tc-sec">
                        <label for="password_confirmation2">{{ __('ui.conferma') }} Password</label>
                        <input type="password" class="form-control" name="password_confirmation"
                            id="password_confirmation2" placeholder="Conferma password">
                    </div>
                    <button type="submit" class="btn btn-custom tc-base">{{ __('ui.registrati') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- fine modal1 -->

{{ $slot }}

<!-- Footer -->
<footer class="footer text-center text-lg-start border-top border-5 rounded-pill ">
    <!-- Grid container -->
    <div class="container p-4 ">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0 ps-0 ps-md-5 text-center">
                <h5 class="text-uppercase tc-accent2">{{ __('ui.titolo footer') }}</h5>

                <p>
                    {{ __('ui.descrizione footer') }}
                </p>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0 ps-0 ps-md-5">
                <h5 class="text-uppercase tc-accent2 text-center">Links</h5>

                <ul class="list-unstyled mb-0 text-center">
                    <li>
                        <a href="{{ route('contact.form') }}" class="text-dark">{{ __('ui.contatti') }}</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">{{ __('ui.chi siamo') }}</a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">{{ __('ui.aiuto') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('article.create') }}" class="text-dark">{{ __('ui.vendi') }} </a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-0 tc-accent2 text-center">Social</h5>

                <ul class="list-unstyled text-center">
                    <li>
                        <a href="#!" class="text-dark">Facebook <i class="fab fa-twitter tc-main mx-2"></i></a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Instagram <i class="fab fa-instagram tc-main mx-2"></i></a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Twitter <i class="fab fa-facebook tc-main mx-2"></i></a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Telegram <i class="fab fa-telegram tc-main mx-2"></i></a>
                    </li>
                    <li>
                        <a href="#!" class="text-dark">Linkedin <i class="fab fa-linkedin tc-main mx-2"></i></a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2020 Copyright:
        <a class="text-dark" href="https://mdbootstrap.com/">Presto.it</a>
    </div>
    <!-- Copyright -->
</footer>
<!-- Footer -->

</div>
