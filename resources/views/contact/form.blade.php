<x-layout>
    <x-navbar>
      
      <div class="container mt-5 py-5">
        <div class="row">
          <div class="col-12">
            <h2 class="tc-accent text-center category-border"> <span class="tc-sec "> {{__('ui.lavora con')}}</span>  {{__('ui.noi!')}}</h2>
          </div>
        </div>
      </div>
        <div class="container-fluid carrot-bg">
            <div class="row">
                <div class="col-12 col-md-4 offset-md-4 mt-5 card  border-cstm shadow my-5 py-3 px-4">
                    <form method="POST" action="{{route('contact.submit')}}">
                      @if ($errors->any())
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                        @csrf

                        <div class="mb-3 text-center">
                            <label for="exampleInputUser" class="form-label tc-sec">{{__('ui.nome utente')}}</label>
                            <input type="text" name="user" class="form-control" id="exampleInputUser">
                          </div>
                        <div class="mb-3 text-center">
                          <label for="exampleInputEmail1" class="form-label tc-sec">{{__('ui.Indirizzo Email')}}</label>
                          <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputmessage" class="form-label"></label>
                          <textarea name="message" id="exampleInputmessage" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                        <div class="my-3 text-center">
                            <label for="phone" class="tc-sec form-label">Tel</label>
                            <input type="tel" name='phone' class="form-control" id="phone" placeholder="Tel"
                                pattern="[0-9]{10}" aria-describedby=" emailHelp">
                        </div>
                        <div class="d-flex justify-content-center">
                          <button type="submit" class="btn btn-custom tc-base ">Submit</button>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </x-navbar>
</x-layout>