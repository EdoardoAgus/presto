<x-layout>
    <x-navbar>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container mt-5 py-5">
            <div class="row">
              <div class="col-12">
                <h2 class="tc-accent text-center category-border"> <span class="tc-sec "> {{__('ui.Accedi')}}</h2>
              </div>
            </div>
          </div>
        <div class="container-fluid my-5 py-5 carrot-bg">
            <div class="row justify-content-center py-3 mx-2">
                <div class="col-12 col-md-4 border-cstm bg-base text-center">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group py-2">
                            <label for="exampleInputEmail1" class="my-2">{{ __('ui.Indirizzo Email') }}</label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group py-2">
                            <label for="exampleInputPassword1" class="my-2">Password</label>
                            <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                                placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-custom tc-base">{{__('ui.Accedi')}}</button>
                        <div class="modal-footer">
                            <p>{{ __('ui.non sei ancora') }}</p>
                            <a href="{{ route('register') }}" type="button"
                            class="btn btn-custom2 me-2 tc-sec active">
                            {{__('ui.registrati')}}
                        </a> 
            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </x-navbar>
</x-layout>
