<x-layout>
    <x-navbar>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container mt-5 py-5">
            <div class="row">
              <div class="col-12">
                <h2 class="tc-accent text-center category-border"> <span class="tc-sec "> {{__('ui.registrati')}}</h2>
              </div>
            </div>
          </div>
        <div class="container-fluid my-0 py-0 py-md-2 carrot-bg">
            <div class="row justify-content-center py-3 mx-2">
                <div class="col-12 col-md-4 border-cstm bg-base text-center">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group py-2">
                            <label for="name" class="my-2">{{ __('ui.nome utente') }}</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nome e Cognome">
                        </div>
                        <div class="form-group py-2">
                            <label for="exampleInputEmail1" class="my-2">{{ __('ui.Indirizzo Email') }}</label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Email">
                        </div>
                        <div class="form-group py-2">
                            <label for="exampleInputPassword1" class="my-2">Password</label>
                            <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                                placeholder="Password">
                        </div>
                        <div class="form-group py-2">
                            <label for="password_confirmation" class="my-2">{{__('ui.conferma')}} Password</label>
                            <input type="password" class="form-control" name="password_confirmation"
                                id="password_confirmation" placeholder="Conferma password">
                        </div>
                        <button type="submit" class="btn btn-custom tc-base">{{__('ui.registrati')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </x-navbar>
</x-layout>
