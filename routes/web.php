<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RevisorController;
use GuzzleHttp\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');

/* Article Controller*/

Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');

Route::post('/article/store', [ArticleController::class, 'store'])->name('article.store');

Route::get('/article/show/{article}', [ArticleController::class, 'show'])->name('article.show');

Route::delete('/articolo/destroy/{article}', [ArticleController::class, 'destroy'])->name('article.destroy');

// Route::get('/category/articles/{cat}', [PublicController::class, 'cat'])->name('category.articles');
Route::get('/category/{name}/{id}/articles', [PublicController::class, 'index'])->name('category.articles');

//rotte revisor

Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisor.home');
Route::post('/revisor/article/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');
Route::post('/revisor/articles/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');
Route::get('/revisor/trash/', [RevisorController::class, 'trash'])->name('revisor.trash');
Route::post('/revisor/article/{id}/restore', [RevisorController::class, 'restore'])->name('revisor.restore');

// rotta search
Route::get('/search', [PublicController::class, 'search'])->name('search');

//rotta  mail
Route::get('/contact/form', [PublicController::class , 'contacts'])->name('contact.form');
Route::post('/contact/submit', [PublicController::class, 'submit'])->name('contact.submit');

//dropzone
Route::post('/article/images/upload', [ArticleController::class, 'uploadImages'])->name('article.images.upload');
Route::delete('/article/images/remove', [ArticleController::class, 'removeImage'])->name('article.images.remove');
Route::get('/article/images', [ArticleController::class, 'getImages'])->name('article.images');

//rotta lingua
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');

Route::get('/category/{category}', [PublicController::class, 'category'])->name('article.category');